package com.example.bizplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.adapters.ImageViewBindingAdapter;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.bizplay.databinding.ActivityGalleryBinding;
import com.example.bizplay.dialog.CustomImageDialog;

public class Gallery extends AppCompatActivity {

    ActivityGalleryBinding binding;
    GridView gridView;

    private int count;
    private Bitmap[] bitmaps;
    private String[] arrPath;
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);


        gridView = binding.ImageGrid;

        final String[] columns = {MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media._ID;
        Cursor imagecursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                columns, null, null, orderBy);

        int image_column_index = imagecursor.getColumnIndex(MediaStore.Images.Media._ID);
        this.count = imagecursor.getCount();
        this.bitmaps = new Bitmap[this.count];
        this.arrPath = new String[this.count];

        for (int i = 0; i < this.count; i++) {
            imagecursor.moveToPosition(i);
            int id = imagecursor.getInt(image_column_index);
            int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED);///
            bitmaps[i] = MediaStore.Images.Thumbnails.getThumbnail(getApplicationContext()
                    .getContentResolver(), id, MediaStore.Images.Thumbnails.MICRO_KIND, null);
            arrPath[i] = imagecursor.getString(dataColumnIndex);
        }

        GridView imagegrid = binding.ImageGrid;

        imageAdapter = new ImageAdapter();
        imagegrid.setAdapter(imageAdapter);
        imagecursor.close();


    }

    class ImageAdapter extends BaseAdapter {


        private LayoutInflater inflater;
        int count;

        public ImageAdapter() {
//        inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ImageView thumbitems;
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.gallery_items, null);
                holder.imageView = convertView.findViewById(R.id.thumbImage);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int id = v.getId();
                    Intent intent = new Intent(Gallery.this, CustomImageDialog.class);
                    intent.putExtra("path", arrPath[id]);
                    startActivity(intent);

                    }
                });
            holder.imageView.setImageBitmap(bitmaps[position]);

            return convertView;
            }

            return null;
        }

        class ViewHolder {
            ImageView imageView;
        }
    }

}
