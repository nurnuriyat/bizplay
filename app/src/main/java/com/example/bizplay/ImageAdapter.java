package com.example.bizplay;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bizplay.databinding.GalleryItemsBinding;
import com.example.bizplay.dialog.CustomImageDialog;

import java.util.zip.Inflater;

class ImageAdapter  extends BaseAdapter {


    private LayoutInflater inflater;
    int count;
    public ImageAdapter(){
//        inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView thumbitems;
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.gallery_items,null);
            holder.imageView = convertView.findViewById(R.id.thumbImage);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = v.getId();
//                    Intent intent = new Intent(ImageAdapter.this, CustomImageDialog.class);
                }
            });

        }

        return null;
    }

    static class ViewHolder{
        ImageView imageView;
    }
}
