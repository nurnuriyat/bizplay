package com.example.bizplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.bizplay.databinding.ActivityBizplayBinding;

public class IntroActivity extends AppCompatActivity {


    ActivityBizplayBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_bizplay);

        ImageView imag = binding.ivIntro;


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                autoLogin();
            }

        }, 3000);


    }

    public void autoLogin() {

        @SuppressLint("WrongConstant")
        SharedPreferences sharedPreferences = getSharedPreferences("MySharePref", MODE_APPEND);
        String id = sharedPreferences.getString("id", "");
        String pwd = sharedPreferences.getString("pwd", "");

        // check auto login
        if (!"".equals(id) && !"".equals(pwd)) {
            Intent intent = new Intent(IntroActivity.this, OperationActivity.class);
            startActivity(intent);
        } else {
            openLanguageScreen();
        }

    }


    private void openLanguageScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
